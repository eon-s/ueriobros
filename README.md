Proyecto basado en SideScrollerGame

Big Disclaimer: La nomenclatura de archivos no es la correcta para proyectos de UE4

Algunos problemas en el setup del proyecto:
VSCode es usado para el código, este puede generar mal las rutas de includes.
Git LFS se rompió recientemente al querer solucionar un problema común de usuarios de windows, lo que afectó a quienes usan links simbólicos (discos montados)
UFUNCTIONS con BlueprintEditable no se llevan bien con Live Coding habilitado, puede que nunca aparezcan las funciones expuestas 

GameMode: SideScrollerGameMode
Controla el spawn y respawn del SideScrollerCharacter

Se conecta al OnDestroy del player character para respawnear el character (no es el modo ideal de matar el player)

Level Blueprint: SideScrollerExampleMap 
Abre los widgets de game over, está pendiente de player destroy y spawn (esto lo debería hacer game state probablemente)

Actores:

Tube_Blueprint
Actor simple compuesto de meshes que son colliders y triggers usados para detectar punto de ingreso, el egreso es configurable
Esta configuración permite mover la salida a cualquier punto para acomodarla a un nivel (no está 100% bien pero si encaminada a una estructura flexible)

BP_HardBrick
Sólo se mueve cuando le peguen desde abajo, los triggers deberían filtrar por capas para que lo golpee solo player o cualquier golpeabloques

BP_POW
Tiene un intento fallido de camera shake, hace más cosas de las que le corresponde y es muy similar al hardblock, ambos podrían separarse en componentes y compartir partes
Busca a todos los enemigos de una manera poco práctica, sólo debería emitir un evento "kill screen" y y tal vez el level administrar subscripciones de todo lo "killable" o tal vez a (algún componente de) la cámara si sólo afecta lo que está en pantalla

BP_Trap 
Es algo básico que mata el player y cambia de mesh para simular cierre

MovingPlatform_MovingPlatform_Blueprint
Tiene todos nombres autogenerados desde importación hasta el BP, hace nada especial pero...

LS_PlatformUpdownMotion
Este elemento del nivel mueve esa plataforma malnombrada

BP_RedTurtleEnemy
Se puede dar vuelta cuando lo golpean de abajo, puede matar al player o morir según el estado y el origen del golpe (no usa capas, debería)

La lógica de movimiento es simple, si se mueve a la derecha, tira un rayo a la derecha, hacia abajo, y mira si hay piso, si no hay cambia de dirección y hace lo mismo del otro lado (no usa capas correctamente, debería hacerlo)
La lógica se hizo primero en el tick del blueprint principal (actualmente anulada)
La misma lógica se convirtió en componente (le falta funcionalidad visual), que sería lo ideal en este tip'o de motores (actualmente anulada)
La misma lógica está en c++ en forma de componente.

Este modo de uso no está completo, me faltan herramientas para mejorar este workflow
