// Fill out your copyright notice in the Description page of Project Settings.


#include "RedTurtleCPPMovement.h"
#include "DrawDebugHelpers.h"


// Sets default values for this component's properties
URedTurtleCPPMovement::URedTurtleCPPMovement()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void URedTurtleCPPMovement::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void URedTurtleCPPMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsUpsideDown) return;

	UChildActorComponent* checkHelper;
	if (MovementDirection > 0) {
		checkHelper = Right;
	} else {
		checkHelper = Left;
	}

	bool hit = false;
	FVector startPos = checkHelper->GetComponentLocation();
	FVector endPos = checkHelper->GetComponentLocation();
	endPos.Z -= 50.0;
	TArray<FHitResult, FDefaultAllocator> hitResult;

	DrawDebugLine(GetWorld(), startPos, endPos, FColor(255, 0, 0), false, .1f, 0, 12.3f);
	GetWorld()->LineTraceMultiByObjectType(hitResult, startPos, endPos, FCollisionObjectQueryParams(EObjectTypeQuery::ObjectTypeQuery1));

	FVector offset;

	if (hitResult.Num() > 0) {
		offset = FVector(0,-MovementDirection,0);
	} else {
		MovementDirection *= -1;
 		offset = FVector(0,0,-1);
	}

	

	Capsule->AddWorldOffset(offset, true);

}

void URedTurtleCPPMovement::Initialize(bool iud, float md, UCapsuleComponent* collider, UChildActorComponent* l, UChildActorComponent* r)
{
	IsUpsideDown = iud;
	MovementDirection = md;
	Capsule = collider;
	Left = l;
	Right = r;
	
}

