// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/ChildActorComponent.h"
#include "RedTurtleCPPMovement.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UERIOBROS_API URedTurtleCPPMovement : public UActorComponent
{
	GENERATED_BODY()

public:	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
    bool IsUpsideDown;
	UPROPERTY(EditAnywhere)
	float MovementDirection;
	UPROPERTY(EditAnywhere)
	UCapsuleComponent* Capsule;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UChildActorComponent* Left;
	UPROPERTY(EditAnywhere)
	UChildActorComponent* Right;

	//tests
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FComponentReference LeftReference;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent*	m_leftReference;

	URedTurtleCPPMovement();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		
	UFUNCTION(BlueprintCallable)
	void Initialize(bool iud, float md, UCapsuleComponent* collider, UChildActorComponent* l, UChildActorComponent* r);
};
